<?php
/**
 * A-Level kids functions and definitions
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package A-Level_kids
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.3.0' );
}

if ( ! function_exists( 'a_level_kids_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function a_level_kids_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on A-Level kids, use a find and replace
		 * to change 'a-level-kids' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'a-level-kids', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			[
				'main-menu' => esc_html__( 'Main menu', 'a-level-kids' ),
			]
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			[
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			]
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'a_level_kids_custom_background_args',
				[
					'default-color' => 'ffffff',
					'default-image' => '',
				]
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

	}
endif;
add_action( 'after_setup_theme', 'a_level_kids_setup' );


/**
 * Enqueue scripts and styles.
 */
function a_level_kids_scripts() {
	wp_enqueue_style( 'slick-style', get_template_directory_uri() . '/assets/slick/slick.css', [], _S_VERSION );
	wp_enqueue_style( 'slick-theme-style', get_template_directory_uri() . '/assets/slick/slick-theme.css', [], _S_VERSION );
	wp_enqueue_style( 'modal-style', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css', [], _S_VERSION );

	wp_enqueue_style( 'a-level-kids-style', get_stylesheet_uri(), [], _S_VERSION );
	wp_enqueue_style( 'a-level-kids-global-style', get_template_directory_uri() . '/assets/css/global.css', [], _S_VERSION );

	if ( is_page_template( 'templates/template-home.php' ) ) {
		wp_enqueue_style( 'a-level-kids-home-style', get_template_directory_uri() . '/assets/css/style.css', [], _S_VERSION );
	}

	if ( is_page_template( 'templates/template-feedbacks.php' ) ) {
		wp_enqueue_style( 'a-level-kids-feedbacks-style', get_template_directory_uri() . '/assets/css/feedbacks.css', [], _S_VERSION );
	}

	if ( is_page_template( 'templates/template-contacts.php' ) ) {
		wp_enqueue_style( 'a-level-kids-contacts-style', get_template_directory_uri() . '/assets/css/contact.css', [], _S_VERSION );
	}

	if ( is_page_template( 'templates/template-course.php' ) ) {
		wp_enqueue_style( 'a-level-kids-course-style', get_template_directory_uri() . '/assets/css/course.css', [], _S_VERSION );
	}

	if ( is_page_template( 'templates/template-course-test.php' ) ) {
		wp_enqueue_style( 'a-level-kids-course-style', get_template_directory_uri() . '/assets/css/course.css', [], _S_VERSION );
	}

	wp_enqueue_style( 'a-level-kids-fonts-style', get_template_directory_uri() . '/assets/fonts/stylesheet.css', [], _S_VERSION );
	wp_enqueue_style( 'a-level-kids-style', get_template_directory_uri() . '/style.css', [], _S_VERSION );

	wp_enqueue_script( 'modal-js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js', [ 'jquery' ], _S_VERSION, true );
	wp_enqueue_script( 'mask-js', get_template_directory_uri() . '/assets/js/inputmask/jquery.inputmask.min.js', [ 'jquery' ], _S_VERSION, true );
	wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/assets/slick/slick.min.js', [ 'jquery' ], _S_VERSION, true );
	wp_enqueue_script( 'a-level-kids-main-js', get_template_directory_uri() . '/assets/js/main.js', [ 'jquery' ], _S_VERSION, true );

}

add_action( 'wp_enqueue_scripts', 'a_level_kids_scripts' );

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';


use Carbon_Fields\Carbon_Fields;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * Create Carbon fields settings.
 */
function crb_attach_theme_options() {
	Container::make(
		'theme_options',
		__( 'Theme Options', 'a-level-kids' )
	)->add_tab(
		__( 'Contacts', 'a-level-kids' ),
		[
			Field::make(
				'textarea',
				'crb_contact',
				__( 'Contact info', 'a-level-kids' )
			)->set_default_value( 'г. Харьков, пл. Павловская, 6 <br>+38 (098) 569 76 76' )->set_width( 50 ),
			Field::make(
				'text',
				'crb_phone',
				__( 'Phone', 'a-level-kids' )
			)->set_default_value( '+38 (066) 525 82 04' )->set_width( 100 ),
		]
	)->add_tab(
		__( 'Socials', 'a-level-kids' ),
		[
			Field::make(
				'text',
				'crb_inst',
				__( 'Instagram', 'a-level-kids' )
			)->set_default_value( '#' )->set_width( 50 ),
			Field::make(
				'text',
				'crb_facebook',
				__( 'Facebook', 'a-level-kids' )
			)->set_default_value( '#' )->set_width( 50 ),
		]
	)->add_tab(
		__( 'Feedbacks', 'a-level-kids' ),
		[
			Field::make(
				'complex',
				'crb_feedbacks',
				__( 'Feedbacks', 'a-level-kids' )
			)->add_fields(
				[
					Field::make( 'image', 'photo', __( 'Image', 'a-level-kids' ) )->set_width( 33 ),
					Field::make( 'text', 'name', __( 'Name', 'a-level-kids' ) )->set_width( 33 ),
					Field::make( 'date', 'date', __( 'Date', 'a-level-kids' ) )->set_storage_format( 'd.m.Y' )
						->set_width( 33 ),
					Field::make( 'rich_text', 'text', __( 'Text', 'a-level-kids' ) ),
				]
			),
		]
	)->add_tab(
		__( 'Form (остались вопросы?)', 'a-level-kids' ),
		[
			Field::make( 'text', 'form_title', __( 'Title', 'a-level-kids' ) )
				->set_default_value( 'остались вопросы?' )
				->set_width( 50 ),
			Field::make( 'textarea', 'form_text', __( 'Text' ) )
				->set_default_value( 'Администратор с удовольствием ответит на все вопросы и проконсультирует по поводу занятий. Все, что нужно - это заполнить форму' )
				->set_width( 50 ),
			Field::make( 'rich_text', 'crb_contact_form', __( 'Title', 'a-level-kids' ) )
				->set_default_value( '<p class="contact">Или можете набрать нас сами по номерам:<strong><br> +38 (068) 811 32 62; +38 (050) 144 37 32</strong></p><p>Мы работаем:<strong>Пн-Пт с 9.00 до 18.00; Сб-Вс с 9.00 до 22.00</strong></p>' ),
		]
	)->add_tab(
		__( 'Pop-up Tnx', 'a-level-kids' ),
		[
			Field::make( 'text', 'course_title_tnx', __( 'Course Title Tnx', 'a-level-kids' ) )
				->set_default_value( 'Заголовок' )
				->set_width( 50 ),
			Field::make( 'textarea', 'course_text_tnx', __( 'Text' ) )
				->set_default_value( 'Текст' )
				->set_width( 50 ),
			Field::make( 'text', 'course_free_title_tnx', __( 'Course Free Title Tnx', 'a-level-kids' ) )
				->set_default_value( 'Заголовок' )
				->set_width( 50 ),
			Field::make( 'textarea', 'course_free_text_tnx', __( 'Text' ) )
				->set_default_value( 'Текст' )
				->set_width( 50 ),
			Field::make( 'text', 'remain_title_tnx', __( 'Remain Title Tnx', 'a-level-kids' ) )
				->set_default_value( 'Заголовок' )
				->set_width( 50 ),
			Field::make( 'textarea', 'remain_free_text_tnx', __( 'Text' ) )
				->set_default_value( 'Текст' )
				->set_width( 50 ),
		]
	);

	Container::make( 'post_meta', 'Custom Data' )
		->where( 'post_type', '=', 'page' )
		->where( 'post_template', '=', 'templates/template-home.php' )
		->add_tab(
			__( 'First screen', 'a-level-kids' ),
			[
				Field::make( 'rich_text', 'crb_title', __( 'Title', 'a-level-kids' ) )
					->set_default_value( '<h1>КУРСЫ ДЛЯ ШКОЛЬНИКОВ НА КОНСТРУКТОРАХ LEGO</h1><p>Играй, развивайся, учись строить и програмировать вместе с нами!</p>' ),
				Field::make(
					'complex',
					'crb_courses',
					__( 'Courses', 'a-level-kids' )
				)->add_fields(
					[
						Field::make( 'association', 'course', __( 'Course', 'a-level-kids' ) )
							->set_types(
								[
									[
										'type'      => 'post',
										'post_type' => 'page',
									],
								]
							)
							->set_min( 1 )
							->set_max( 1 )
							->set_width( 50 ),
						Field::make( 'date', 'date', __( 'Date', 'a-level-kids' ) )
							->set_storage_format( 'd.m.Y' )
							->set_width( 50 ),
					]
				),
				Field::make( 'file', 'crb_video', __( 'Video Main banner', 'a-level-kids' ) ),
				Field::make( 'image', 'banner_desktop', __( 'Image Desktop', 'a-level-kids' ) )
					->set_width( 50 ),
				Field::make( 'image', 'banner_mobile', __( 'Image Mobile', 'a-level-kids' ) )
					->set_width( 50 ),
			]
		)->add_tab(
			__( 'Second screen', 'a-level-kids' ),
			[
				Field::make( 'text', 'crb_title_second', __( 'Title', 'a-level-kids' ) )
					->set_default_value( 'КУРСЫ ДЛЯ ШКОЛЬНИКОВ A-LEVEL' )
					->set_width( 50 ),
				Field::make( 'image', 'photo_second', __( 'Image', 'a-level-kids' ) )
					->set_width( 50 ),
				Field::make( 'rich_text', 'crb_info_second', __( 'Content', 'a-level-kids' ) ),
			]
		)->add_tab(
			__( 'Third screen', 'a-level-kids' ),
			[
				Field::make( 'complex', 'crb_benefits', __( 'Benefits', 'a-level-kids' ) )
					->add_fields(
						[
							Field::make( 'text', 'title', __( 'Title', 'a-level-kids' ) )
								->set_width( 50 ),
							Field::make( 'textarea', 'text', __( 'Text', 'a-level-kids' ) )
								->set_width( 50 ),
						]
					),
			]
		)->add_tab(
			__( 'Fourth screen', 'a-level-kids' ),
			[
				Field::make( 'rich_text', 'crb_content_fourth', __( 'Content', 'a-level-kids' ) ),
			]
		)->add_tab(
			__( 'JUNIOR (ДЕТИ 6-10 ЛЕТ)', 'a-level-kids' ),
			[
				Field::make( 'text', 'crb_title_junior', __( 'Title', 'a-level-kids' ) )
					->set_default_value( 'JUNIOR (ДЕТИ 6-10 ЛЕТ)' )
					->set_width( 33 ),
				Field::make( 'image', 'photo_junior', __( 'Image', 'a-level-kids' ) )
					->set_width( 33 ),
				Field::make( 'text', 'link_junior', __( 'Link', 'a-level-kids' ) )
					->set_width( 33 ),
				Field::make( 'rich_text', 'crb_info_1_junior', __( 'Content', 'a-level-kids' ) )
					->set_width( 50 ),
				Field::make( 'rich_text', 'crb_info_2_junior', __( 'Content' ) )
					->set_width( 50 ),
			]
		)->add_tab(
			__( 'SENIOR (ДЕТИ 10-14 ЛЕТ)', 'a-level-kids' ),
			[
				Field::make( 'text', 'crb_title_senior', __( 'Title', 'a-level-kids' ) )
					->set_default_value( 'SENIOR (ДЕТИ 10-14 ЛЕТ)' )
					->set_width( 33 ),
				Field::make( 'image', 'photo_senior', __( 'Image', 'a-level-kids' ) )
					->set_width( 33 ),
				Field::make( 'text', 'link_senior', __( 'Link', 'a-level-kids' ) )
					->set_width( 33 ),
				Field::make( 'rich_text', 'crb_info_1_senior', __( 'Content', 'a-level-kids' ) )
					->set_width( 50 ),
				Field::make( 'rich_text', 'crb_info_2_senior', __( 'Content', 'a-level-kids' ) )
					->set_width( 50 ),
			]
		)->add_tab(
			__( 'Six screen', 'a-level-kids' ),
			[
				Field::make( 'text', 'crb_title_six_1', __( 'Title', 'a-level-kids' ) )
					->set_default_value( 'ПРОФЕССИЯ БУДУЩЕГО' )
					->set_width( 33 ),
				Field::make( 'text', 'crb_title_six_2', __( 'Title', 'a-level-kids' ) )
					->set_default_value( 'ЦЕЛЬ - УСПЕХ В ЖИЗНИ' )
					->set_width( 33 ),
				Field::make( 'text', 'crb_title_six_3', __( 'Title', 'a-level-kids' ) )
					->set_default_value( 'ГАДЖЕТЫ С ПОЛЬЗОЙ' )
					->set_width( 33 ),
				Field::make( 'textarea', 'crb_text_six_1', __( 'Text', 'a-level-kids' ) )
					->set_default_value( 'Робототехника уже заменяет многие профессии по всему миру. Мы первые, кто предлагает полноценно освоить эту профессию уже сейчас! Также это уникальная возможность познакомиться с IT индустрией, куда с каждым годом становится все труднее попасть новичку.' )
					->set_width( 33 ),
				Field::make( 'textarea', 'crb_text_six_2', __( 'Text', 'a-level-kids' ) )
					->set_default_value( 'Уже с юных лет ребенок получает практические навыки и понимание, для чего они нужны и где применяются. Каждые 2 месяца дети смогут применить полученные знания, выполняя собственный проект, который будет полезен в быту или в обществе.' )
					->set_width( 33 ),
				Field::make( 'textarea', 'crb_text_six_3', __( 'Text', 'a-level-kids' ) )
					->set_default_value( 'От технологического прогресса уже никак не уйти. Да и не нужно! Мы используем современные гаджеты не только для игр и развлечений. Мы научим как правильно их применять в обучении и дальнейшей работе.' )
					->set_width( 33 ),
			]
		);

	Container::make( 'post_meta', 'Custom Data' )
		->where( 'post_type', '=', 'page' )
		->where( 'post_template', '=', 'templates/template-course.php' )
		->add_tab(
			__( 'First screen', 'a-level-kids' ),
			[
				Field::make( 'rich_text', 'crb_title', __( 'Title', 'a-level-kids' ) )
					->set_default_value( '<h1>КУРС JUNIOR ДЛЯ ДЕТЕЙ 6-10 ЛЕТ</h1><p>Подарите возможность своему ребенку с раннего возраста получить навыки на всю жизнь</p>' ),
				Field::make( 'text', 'crb_cat', __( 'Category', 'a-level-kids' ) )
					->set_default_value( 'ДЕТИ 6-10 ЛЕТ' ),
				Field::make( 'image', 'crb_banner', __( 'Banner', 'a-level-kids' ) )->set_value_type( 'url' ),

				Field::make(
					'complex',
					'crb_courses',
					__( 'Courses', 'a-level-kids' )
				)->add_fields(
					[
						Field::make( 'association', 'course', __( 'Course', 'a-level-kids' ) )
							->set_types(
								[
									[
										'type'      => 'post',
										'post_type' => 'page',
									],
								]
							)
							->set_min( 1 )
							->set_max( 1 )
							->set_width( 50 ),
						Field::make( 'date', 'date', __( 'Date', 'a-level-kids' ) )
							->set_storage_format( 'd.m.Y' )
							->set_width( 50 ),
					]
				),
			]
		)->add_tab(
			__( 'Second screen', 'a-level-kids' ),
			[
				Field::make( 'text', 'crb_title_second', __( 'Title', 'a-level-kids' ) )
					->set_default_value( 'курс JunIor' )
					->set_width( 50 ),
				Field::make( 'image', 'photo_second', __( 'Image', 'a-level-kids' ) )
					->set_width( 50 ),
				Field::make( 'rich_text', 'crb_info_second', __( 'Content', 'a-level-kids' ) ),
			]
		)->add_tab(
			__( 'Third screen', 'a-level-kids' ),
			[
				Field::make( 'rich_text', 'crb_content_fourth', __( 'Content', 'a-level-kids' ) ),
			]
		)->add_tab(
			__( 'Fourth screen', 'a-level-kids' ),
			[
				Field::make( 'complex', 'crb_courses_list', __( 'Courses', 'a-level-kids' ) )
					->add_fields(
						[
							Field::make( 'text', 'crb_title', __( 'Title', 'a-level-kids' ) )
								->set_default_value( 'УЧЕБНАЯ ПРОГРАММА' )
								->set_width( 33 ),
							Field::make( 'complex', 'crb_programs', __( 'Programs', 'a-level-kids' ) )
								->add_fields(
									[
										Field::make( 'image', 'photo', __( 'Image', 'a-level-kids' ) ),
										Field::make( 'rich_text', 'content', __( 'Content', 'a-level-kids' ) ),
									]
								),
							Field::make( 'text', 'crb_title_f_1', __( 'Title', 'a-level-kids' ) )
								->set_default_value( 'ДЛИТЕЛЬНОСТЬ КУРСА' )
								->set_width( 33 ),
							Field::make( 'text', 'crb_title_f_2', __( 'Title', 'a-level-kids' ) )
								->set_default_value( 'СТОИМОСТЬ КУРСА' )
								->set_width( 33 ),
							Field::make( 'text', 'crb_title_f_3', __( 'Title', 'a-level-kids' ) )
								->set_default_value( 'ДАТА СТАРТА' )
								->set_width( 33 ),
							Field::make( 'textarea', 'crb_text_f_1', __( 'Text', 'a-level-kids' ) )
								->set_default_value( 'Курс длится 8 недель. Занятия проходят 2 раза в неделю по 1,5 часа.' )
								->set_width( 33 ),
							Field::make( 'textarea', 'crb_text_f_2', __( 'Text', 'a-level-kids' ) )
								->set_default_value( 'Стоимость курса зависит от количества уроков. Один урок стоит 350 грн.' )
								->set_width( 33 ),
							Field::make( 'textarea', 'crb_text_f_3', __( 'Text', 'a-level-kids' ) )
								->set_default_value( 'Согласовывается индивидуально. Новичок может подключиться в любой момент.' )
								->set_width( 33 ),
						]
					),
			]
		);

	Container::make( 'post_meta', 'Custom Data' )
		->where( 'post_type', '=', 'page' )
		->where( 'post_template', '=', 'templates/template-course-test.php' )
		->add_tab(
			__( 'First screen', 'a-level-kids' ),
			[
				Field::make( 'rich_text', 'crb_title', __( 'Title', 'a-level-kids' ) )
					->set_default_value( '<h1>КУРС JUNIOR ДЛЯ ДЕТЕЙ 6-10 ЛЕТ</h1><p>Подарите возможность своему ребенку с раннего возраста получить навыки на всю жизнь</p>' ),
				Field::make( 'text', 'crb_cat', __( 'Category', 'a-level-kids' ) )
					->set_default_value( 'ДЕТИ 6-10 ЛЕТ' ),
				Field::make( 'image', 'crb_banner', __( 'Banner', 'a-level-kids' ) )
					->set_value_type( 'url' ),
				Field::make(
					'complex',
					'crb_courses',
					__( 'Courses', 'a-level-kids' )
				)->add_fields(
					[
						Field::make( 'association', 'course', __( 'Course', 'a-level-kids' ) )
							->set_types(
								[
									[
										'type'      => 'post',
										'post_type' => 'page',
									],
								]
							)
							->set_min( 1 )
							->set_max( 1 )
							->set_width( 50 ),
						Field::make( 'date', 'date', __( 'Date', 'a-level-kids' ) )
							->set_storage_format( 'd.m.Y' )
							->set_width( 50 ),
					]
				),
			]
		)->add_tab(
			__( 'Second screen', 'a-level-kids' ),
			[
				Field::make( 'text', 'crb_title_second', __( 'Title', 'a-level-kids' ) )
					->set_default_value( 'курс JunIor' )
					->set_width( 50 ),
				Field::make( 'image', 'photo_second', __( 'Image', 'a-level-kids' ) )
					->set_width( 50 ),
				Field::make( 'rich_text', 'crb_info_second', __( 'Content', 'a-level-kids' ) ),
			]
		);

	Container::make( 'post_meta', 'Custom Data' )
		->where( 'post_type', '=', 'page' )
		->where( 'post_template', '=', 'templates/template-feedbacks.php' )
		->add_tab(
			__( 'First screen', 'a-level-kids' ),
			[
				Field::make( 'rich_text', 'crb_title', __( 'Title', 'a-level-kids' ) )
					->set_default_value( '<h1>ОТЗЫВЫ О КУРСАХ</h1><p>Самое ценное для нас - это отзывы от наших юных студентов и их родителей</p>' ),
				Field::make(
					'complex',
					'crb_courses',
					__( 'Courses', 'a-level-kids' )
				)->add_fields(
					[
						Field::make( 'association', 'course', __( 'Course', 'a-level-kids' ) )
							->set_types(
								[
									[
										'type'      => 'post',
										'post_type' => 'page',
									],
								]
							)
							->set_min( 1 )
							->set_max( 1 )
							->set_width( 50 ),
						Field::make( 'date', 'date', __( 'Date', 'a-level-kids' ) )
							->set_storage_format( 'd.m.Y' )
							->set_width( 50 ),
					]
				),
			]
		);

	Container::make( 'post_meta', 'Custom Data' )
		->where( 'post_type', '=', 'page' )
		->where( 'post_template', '=', 'templates/template-contacts.php' )
		->add_tab(
			__( 'First screen', 'a-level-kids' ),
			[
				Field::make( 'complex', 'crb_phone', __( 'Phones', 'a-level-kids' ) )
					->add_fields(
						[
							Field::make( 'text', 'text', __( 'Telephone', 'a-level-kids' ) )
								->set_default_value( '+38 (068) 811 32 62' ),
						]
					)->set_width( 50 ),
				Field::make( 'textarea', 'crb_address', __( 'Address', 'a-level-kids' ) )
					->set_default_value( '<p>г.Харьков<br>площадь Павловская, 6,<br>ТД "Павловский", 2 этаж</p>' )
					->set_width( 50 ),
				Field::make( 'textarea', 'crb_time', __( 'Time', 'a-level-kids' ) )
					->set_default_value( '<p>Пн-Пт с 9.00 до 18.00<br>Сб-Вс с 9.00 до 22.00</p>' )
					->set_width( 50 ),
			]
		);
}

add_action( 'carbon_fields_register_fields', 'crb_attach_theme_options' );

/**
 * Add Carbon class.
 */
function crb_load() {
	require_once __DIR__ . '/vendor/autoload.php';
	Carbon_Fields::boot();
}

add_action( 'after_setup_theme', 'crb_load' );


/**
 * Add FB tracer code.
 */
function add_fb_traker_wp_footer() {
	?>
	<script type="text/javascript">
		document.addEventListener( 'wpcf7mailsent', function( event ) {
			fbq( 'track', 'Lead' );
		}, false );
	</script>
	<?php
}

add_action( 'wp_footer', 'add_fb_traker_wp_footer' );


add_action(
	'wp_footer',
	function () {
		echo '<script>
      jQuery(\'.wpcf7-form [type="submit"]\').hide();
      jQuery(\'<button type="submit" class="btn">ОТПРАВИТЬ</button>"\').insertAfter(jQuery(\'.wpcf7-form [type="submit"]\'));
      </script>';
	},
	9999
);
