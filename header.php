<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package A-Level_kids
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>

	<script src="https://kit.fontawesome.com/e4d48337ec.js" crossorigin="anonymous"></script>
	<!-- Facebook Pixel Code -->
	<script>
		! function( f, b, e, v, n, t, s ) {
			if ( f.fbq ) return;
			n = f.fbq = function() {
				n.callMethod ?
					n.callMethod.apply( n, arguments ) : n.queue.push( arguments );
			};
			if ( ! f._fbq ) f._fbq = n;
			n.push = n;
			n.loaded = ! 0;
			n.version = '2.0';
			n.queue = [];
			t = b.createElement( e );
			t.async = ! 0;
			t.src = v;
			s = b.getElementsByTagName( e )[ 0 ];
			s.parentNode.insertBefore( t, s );
		}( window, document, 'script',
			'https://connect.facebook.net/en_US/fbevents.js'
		);
		fbq( 'init', '381603949859070' );
		fbq( 'track', 'PageView' );
	</script>
	<noscript><img height="1" width="1" style="display:none"
				   src="https://www.facebook.com/tr?id=381603949859070&ev=PageView&noscript=1"
		/></noscript>
	<!-- End Facebook Pixel Code -->

	<!-- Google Tag Manager -->
	<script>( function( w, d, s, l, i ) {
			w[ l ] = w[ l ] || [];
			w[ l ].push( {
				'gtm.start':
					new Date().getTime(), event: 'gtm.js'
			} );
			var f = d.getElementsByTagName( s )[ 0 ],
				j = d.createElement( s ), dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore( j, f );
		} )( window, document, 'script', 'dataLayer', 'GTM-P7XS8RV' );</script>
	<!-- End Google Tag Manager -->

</head>
<body <?php body_class(); ?>>

<!-- Google Tag Manager (noscript) -->
<noscript>
	<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P7XS8RV"
			height="0" width="0" style="display:none;visibility:hidden">
	</iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<header id="header">
	<div class="container">
		<div class="left">
			<a href="<?php echo esc_url( get_home_url() ); ?>" class="logo">
				<img
						src="<?php echo esc_url( get_template_directory_uri() . '/assets/img/logo.png' ); ?>"
						alt="A-level logo">
			</a>
		</div>
		<div class="right">
			<?php
			wp_nav_menu(
				[
					'theme_location'  => 'main-menu',
					'container'       => 'nav',
					'container_class' => 'main-nav',
					'menu_class'      => 'main-nav',
				]
			);
			$phone = carbon_get_theme_option( 'crb_phone' );
			?>
			<a
					href="tel:<?php echo esc_html( filter_var( $phone, FILTER_SANITIZE_NUMBER_FLOAT ) ) ?? '#'; ?>"
					class="phone fas fa-phone-alt">
				<?php echo esc_html( $phone ) ?? ''; ?>
			</a>
			<div class="burger-menu">
				<a href="" class="burger-menu_button">
					<spun class="burger-menu_lines"></spun>
					<spun class="burger-menu_lines"></spun>
					<spun class="burger-menu_lines"></spun>
				</a>
				<nav class="burger-menu_nav">
					<?php
					wp_nav_menu(
						[
							'theme_location' => 'main-menu',
						]
					);
					?>
				</nav>
				<div class="burger-menu_overlay"></div>
			</div>
		</div>
	</div>
</header><!--#header -->

