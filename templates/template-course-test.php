<?php
/* Template Name: Course test page
*/

get_header();
?>

	<main class="main course-page-main">
		<section id="first-screen" class="first-screen"
				 style="background-image: linear-gradient(177deg, rgba(0,0,0,0.3) 0%, rgba(0,0,0,0.2) 100%), url(<?php echo carbon_get_the_post_meta( 'crb_banner' ); ?>)">
			<div class="container">
				<div class="top">
					<?php echo wp_kses_post( wpautop( carbon_get_the_post_meta( 'crb_title' ) ) ); ?>
					<ul class="buttons">
						<li>
							<a href="#course-modal" rel="modal:open" class="btn">
								<?php esc_html_e( 'Записаться на курс', 'a-level-kids' ); ?>
							</a>
						</li>
						<li>
							<a href="#course-modal-free" rel="modal:open" class="btn">
								<?php esc_html_e( 'Попробовать бесплатно', 'a-level-kids' ); ?>
							</a>
						</li>
					</ul>
				</div>
				<?php
				$courses = carbon_get_the_post_meta( 'crb_courses' );
				if ( $courses ) :
					?>
					<div class="bottom" id="lesson">
						<h4><?php esc_html_e( 'Запись на открытый урок', 'a-level-kids' ); ?></h4>
						<div class="row cards-slider">
							<?php

							foreach ( $courses as $item ) :
								$course_id = $item['course'][0];

								$date_now = new DateTime();
								$date2    = new DateTime( $item['date'] );

								if ( $date_now < $date2 ) :
									?>
									<div class="slide-box">
										<a href="<?php echo esc_url( get_permalink( $course_id['id'] ) ); ?>"
										   class="card">
											<div class="title">
												<span><?php echo esc_html( carbon_get_post_meta( $course_id['id'], 'crb_cat' ) ); ?></span>
											</div>
											<div class="content">
												<div class="card-top">
													<div class="left">
														<h6>
															<?php echo esc_html( get_the_title( $course_id['id'] ) ); ?>
														</h6>
													</div>
													<div class="right">
														<p><?php echo esc_html( $item['date'] ); ?></p>
													</div>
												</div>
												<div class="card-bottom">
													<?php echo get_the_post_thumbnail( $course_id['id'], 'full' ); ?>
												</div>
											</div>
										</a>
									</div>
								<?php
								endif;
							endforeach;
							?>
						</div>
					</div>
				<?php endif; ?>
			</div>

		</section><!-- #first-screen -->

		<div id="course-modal" class="modal">
			<div class="form-box">
				<h4><?php esc_html_e( 'Отправьте заявку', 'a-level-kids' ); ?></h4>
				<p><?php esc_html_e( 'наш менеджер свяжется с Вами в ближайшее время', 'a-level-kids' ); ?></p>
				<?php echo do_shortcode( '[contact-form-7 id="47" title="From course"]' ); ?>
			</div>
		</div>

		<div id="course-modal-free" class="modal">
			<div class="form-box">
				<h4><?php esc_html_e( 'Попробовать бесплатно', 'a-level-kids' ); ?></h4>
				<p><?php esc_html_e( 'наш менеджер свяжется с Вами в ближайшее время', 'a-level-kids' ); ?></p>
				<?php echo do_shortcode( '[contact-form-7 id="263" title="From course free"]' ); ?>
			</div>
		</div>

		<div id="course-modal-tnx" class="modal">
			<div class="form-box">
				<h4><?php echo esc_html( carbon_get_theme_option( 'course_title_tnx' ) ); ?></h4>
				<p><?php echo wp_kses_post( carbon_get_theme_option( 'course_text_tnx' ) ); ?></p>
			</div>
		</div>

		<div id="course-modal-free-tnx" class="modal">
			<div class="form-box">
				<h4><?php echo esc_html( carbon_get_theme_option( 'course_free_title_tnx' ) ); ?></h4>
				<p><?php echo wp_kses_post( carbon_get_theme_option( 'course_free_text_tnx' ) ); ?></p>
			</div>
		</div>

		<section id="about-screen" class="about-screen">
			<div class="container">
				<h3><?php echo esc_html( carbon_get_the_post_meta( 'crb_title_second' ) ); ?></h3>
				<div class="content">
					<div class="left">
						<?php echo wp_get_attachment_image( carbon_get_the_post_meta( 'photo_second' ), 'full' ); ?>
					</div>
					<div class="right">
						<?php echo wp_kses_post( wpautop( carbon_get_the_post_meta( 'crb_info_second' ) ) ); ?>

						<br>
						<ul class="buttons">
							<li>
								<a href="#course-modal" rel="modal:open" class="btn">
									<?php esc_html_e( 'Записаться на курс', 'a-level-kids' ); ?>
								</a>
							</li>
							<li>
								<a href="#course-modal-free" rel="modal:open" class="btn">
									<?php esc_html_e( 'Попробовать бесплатно', 'a-level-kids' ); ?>
								</a>
							</li>
						</ul>

					</div>
				</div>
			</div>
		</section><!-- #about-scree -->


		<?php get_template_part( 'template-parts/content', 'form' ); ?>

		<?php get_template_part( 'template-parts/content', 'feedbacks' ); ?>

		<section id="lesson" class="lesson">
			<div class="container">
				<div class="top">
					<h3>Запись на открытый урок</h3>
					<p>
						Цель открытого урока - познакомить юного студента с атмосферой на курсах и вызвать интерес к
						тому, что его дальше здесь ждет. Открытые уроки бесплатны и их может посетить любой
						желающий.</p>
				</div>
				<div class="bottom">
					<div class="row cards-slider">
						<?php
						$courses = carbon_get_post_meta( 5, 'crb_courses' );

						foreach ( $courses as $item ) :
							$id = $item['course'][0];

							$date_now = new DateTime();
							$date2    = new DateTime( $item['date'] );

							if ( $date_now < $date2 ) :
								?>
								<div class="slide-box">
									<a href="<?php echo esc_html( get_permalink( $id['id'] ) ); ?>" class="card">
										<div class="title">
											<span><?php echo esc_html( carbon_get_post_meta( $id['id'], 'crb_cat' ) ); ?></span>
										</div>
										<div class="content">
											<div class="card-top">
												<div class="left">
													<h6>
														<?php echo esc_html( get_the_title( $id['id'] ) ); ?>
													</h6>
												</div>
												<div class="right">
													<p><?php echo esc_html( $item['date'] ); ?></p>
												</div>
											</div>
											<div class="card-bottom">
												<?php echo get_the_post_thumbnail( $id['id'], 'full' ); ?>
											</div>
										</div>
									</a>
								</div>
							<?php
							endif;
						endforeach;
						?>
					</div>
				</div>

			</div>
		</section><!-- #lesson -->
	</main><!-- .main -->

<?php
get_footer();
