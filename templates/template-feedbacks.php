<?php
/* Template Name: Feedbacks page
*/

get_header();
?>

	<main class="main feedbacks-page-main">
		<section
				class="first-screen"
				style="background-image: linear-gradient(177deg, rgba(0,0,0,0.3) 0%, rgba(0,0,0,0.2) 100%), url
						(<?php echo esc_url( the_post_thumbnail_url( 'full' ) ); ?>)">
			<div class="container">
				<div class="content">
					<?php echo wp_kses_post( wpautop( carbon_get_the_post_meta( 'crb_title' ) ) ); ?>
				</div>
				<?php
				$courses = carbon_get_the_post_meta( 'crb_courses' );
				if ( $courses ) :
					?>
					<div class="bottom" id="lesson">
						<h4><?php esc_html_e( 'Запись на открытый урок', 'a-level-kids' ); ?></h4>
						<div class="row cards-slider">
							<?php

							foreach ( $courses as $item ) :
								$course_id = $item['course'][0];

								$date_now = new DateTime();
								$date2    = new DateTime( $item['date'] );

								if ( $date_now < $date2 ) :
									?>
									<div class="slide-box">
										<a href="<?php echo esc_url( get_permalink( $course_id['id'] ) ); ?>"
										   class="card">
											<div class="title">
												<span><?php echo esc_html( carbon_get_post_meta( $course_id['id'], 'crb_cat' ) ); ?></span>
											</div>
											<div class="content">
												<div class="card-top">
													<div class="left">
														<h6>
															<?php echo esc_html( get_the_title( $course_id['id'] ) ); ?>
														</h6>
													</div>
													<div class="right">
														<p><?php echo esc_html( $item['date'] ); ?></p>
													</div>
												</div>
												<div class="card-bottom">
													<?php echo get_the_post_thumbnail( $course_id['id'], 'full' ); ?>
												</div>
											</div>
										</a>
									</div>
								<?php
								endif;
							endforeach;
							?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</section><!--first-screen -->

		<section class="second-screen">
			<div class="container">
				<h3>ЧТО О НАС ГОВОРЯТ</h3>
				<div class="row">
					<?php
					$feedbacks = carbon_get_theme_option( 'crb_feedbacks' );

					foreach ( $feedbacks as $item ) : ?>
						<div class="col">
							<div class="top">
								<div class="left">
									<?php echo wp_get_attachment_image( $item['photo'], 'full' ); ?>

									<div class="box">
										<p><?php echo esc_html( $item['name'] ); ?></p>
									</div>

								</div>
								<div class="right">
									<p><?php echo esc_html( $item['date'] ); ?></p>
								</div>
							</div>
							<div class="bottom">
								<?php echo wp_kses_post( wpautop( $item['text'] ) ); ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</section><!--second-screen -->

		<section class="third-screen">
			<div class="container">
				<div class="content">

					<h3>ПОДЕЛИТЕСЬ ВПЕЧАТЛЕНИЕМ</h3>
					<p>
						Для нас очень важен каждый отзыв. А самое главное - впечатление, которое получают наши юные
						студенты во время занятий.
					</p>

					<div class="box-form">
						<?php echo do_shortcode( '[contact-form-7 id="34" title="Form feedback big"]' ) ?>
					</div>
				</div>
			</div>
		</section><!--third-screen -->

		<section class="fourth-screen lesson">
			<div class="container">
				<div class="top">
					<h3>БЛИЖАЙШИЕ ОТКРЫТЫЕ УРОКИ</h3>
					<p>
						Цель открытого урока - познакомить юного студента с атмосферой на курсах и вызвать интерес к
						тому, что его дальше здесь ждет. Открытые уроки бесплатны и их может посетить любой
						желающий.</p>
				</div>
				<div class="bottom">
					<div class="row cards-slider">
						<?php
						$courses = carbon_get_post_meta( 5, 'crb_courses' );

						foreach ( $courses as $item ) :
							$id = $item['course'][0];

							$date_now = new DateTime();
							$date2    = new DateTime( $item['date'] );

							if ( $date_now < $date2 ) : ?>
								<div class="slide-box">
									<a href="<?php echo esc_url( get_permalink( $id['id'] ) ); ?>" class="card">
										<div class="title">
											<span><?php echo esc_html( carbon_get_post_meta( $id['id'], 'crb_cat' ) ); ?></span>
										</div>
										<div class="content">
											<div class="card-top">
												<div class="left">
													<h6>
														<?php echo esc_html( get_the_title( $id['id'] ) ); ?>
													</h6>
												</div>
												<div class="right">
													<p><?php echo esc_html( $item['date'] ); ?></p>
												</div>
											</div>
											<div class="card-bottom">
												<?php echo get_the_post_thumbnail( $id['id'], 'full' ); ?>
											</div>
										</div>
									</a>
								</div>
							<?php
							endif;
						endforeach;
						?>
					</div>
				</div>

			</div>
		</section><!--lesson -->
	</main><!-- .main -->

<?php
get_footer();
