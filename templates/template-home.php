<?php
/* Template Name: Home page
*/

get_header();
$video  = carbon_get_the_post_meta( 'crb_video' );
$banner = wp_get_attachment_url( carbon_get_the_post_meta( 'banner_desktop' ) );
if ( ! empty( $banner ) ) {
	$banner_mobile = wp_get_attachment_url( carbon_get_the_post_meta( 'banner_mobile' ) );
} else {
	$banner = get_the_post_thumbnail_url( 'full' );
}
?>

	<main class="main home-page-main">
		<section
				id="first-screen" class="first-screen"
				style="--background-mobile: url('<?php echo esc_url( $banner_mobile ); ?>'); background-image: linear-gradient(177deg, rgba(0,0,0,0.3) 0%, rgba(0,0,0,0.2) 100%), url(<?php echo empty( $video ) ? esc_url( $banner ) : ''; ?>)">
			<?php if ( ! empty( $video ) ) : ?>
				<video
						id="main-video"
						poster="<?php echo esc_url( get_stylesheet_directory_uri() . '/assets/img/screen.jpg' ); ?>"
						autoplay="autoplay" muted loop>
					<source src="<?php echo esc_url( wp_get_attachment_url( $video ) ); ?>" type='video/mp4'>
				</video>
				<a href="#" class="btn-sound"><i class="fas fa-volume-mute"></i></a>
			<?php endif; ?>
			<div class="container">
				<div class="top top-title">
					<?php echo wp_kses_post( wpautop( carbon_get_the_post_meta( 'crb_title' ) ) ); ?>

					<div class="btns-row">
						<ul class="buttons">
							<li>
								<a href="#course-modal" rel="modal:open" class="btn">
									<?php esc_html_e( 'Записаться на курс', 'a-level-kids' ); ?>
								</a>
							</li>
							<li>
								<a href="#course-modal-free" rel="modal:open" class="btn">
									<?php esc_html_e( 'Попробовать бесплатно', 'a-level-kids' ); ?>
								</a>
							</li>
						</ul>
					</div>

					<div id="course-modal" class="modal">
						<div class="form-box">
							<h4><?php esc_html_e( 'Отправьте заявку', 'a-level-kids' ); ?></h4>
							<p><?php esc_html_e( 'наш менеджер свяжется с Вами в ближайшее время', 'a-level-kids' ); ?></p>
							<?php echo do_shortcode( '[contact-form-7 id="47" title="From course"]' ); ?>
						</div>
					</div>

					<div id="course-modal-free" class="modal">
						<div class="form-box">
							<h4><?php esc_html_e( 'Попробовать бесплатно', 'a-level-kids' ); ?></h4>
							<p><?php esc_html_e( 'наш менеджер свяжется с Вами в ближайшее время', 'a-level-kids' ); ?></p>
							<?php echo do_shortcode( '[contact-form-7 id="263" title="From course free"]' ); ?>
						</div>
					</div>

					<div id="course-modal-tnx" class="modal">
						<div class="form-box">
							<h4><?php echo esc_html( carbon_get_theme_option( 'course_title_tnx' ) ); ?></h4>
							<p><?php echo wp_kses_post( carbon_get_theme_option( 'course_text_tnx' ) ); ?></p>
						</div>
					</div>

					<div id="course-modal-free-tnx" class="modal">
						<div class="form-box">
							<h4><?php echo esc_html( carbon_get_theme_option( 'course_free_title_tnx' ) ); ?></h4>
							<p><?php echo wp_kses_post( carbon_get_theme_option( 'course_free_text_tnx' ) ); ?></p>
						</div>
					</div>

				</div>

				<div class="bottom" id="lesson">
					<h4>Запись на открытый урок</h4>
					<div class="row cards-slider">
						<?php
						$courses = carbon_get_the_post_meta( 'crb_courses' );

						foreach ( $courses as $item ) :
							$id = $item['course'][0];

							$date_now = new DateTime();
							$date2    = new DateTime( $item['date'] );

							if ( $date_now < $date2 ) : ?>
								<div class="slide-box">
									<a href="<?php echo esc_url( get_permalink( $id['id'] ) ); ?>" class="card">
										<div class="title">
											<span><?php echo esc_html( carbon_get_post_meta( $id['id'], 'crb_cat' ) ); ?></span>
										</div>
										<div class="content">
											<div class="card-top">
												<div class="left">
													<h6>
														<?php echo esc_html( get_the_title( $id['id'] ) ); ?>
													</h6>
												</div>
												<div class="right">
													<p><?php echo esc_html( $item['date'] ); ?></p>
												</div>
											</div>
											<div class="card-bottom">
												<?php echo get_the_post_thumbnail( $id['id'], 'full' ); ?>
											</div>
										</div>
									</a>
								</div>
							<?php
							endif;
						endforeach;
						?>

					</div>
				</div>
			</div>
		</section> <!--first-screen -->

		<section id="about-screen" class="about-screen">
			<div class="container">
				<h3><?php echo esc_html( carbon_get_the_post_meta( 'crb_title_second' ) ); ?></h3>
				<div class="content">
					<div class="left">
						<?php echo wp_get_attachment_image( carbon_get_the_post_meta( 'photo_second' ), 'full' ); ?>
					</div>
					<div class="right">
						<?php echo wp_kses_post( wpautop( carbon_get_the_post_meta( 'crb_info_second' ) ) ); ?>
					</div>
				</div>
			</div>
		</section><!-- #about-scree -->

		<section id="third-screen" class="third-screen">
			<div class="container">
				<h3>НАШИ ПРЕИМУЩЕСТВА</h3>
				<div class="advantages-list">
					<?php
					$benefits = carbon_get_the_post_meta( 'crb_benefits' );

					foreach ( $benefits as $item ) :
						?>
						<div class="box">
							<div class="left">
								<div class="icon-box"></div>
							</div>
							<div class="right">
								<h5><?php echo esc_html( $item['title'] ); ?></h5>
								<p><?php echo esc_html( $item['text'] ); ?></p>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</section><!-- #third-screen -->

		<section id="target-screen" class="target-screen">
			<div class="container">
				<div class="content">
					<?php echo wp_kses_post( wpautop( carbon_get_the_post_meta( 'crb_content_fourth' ) ) ); ?>
				</div>
			</div>
		</section><!-- #target-screen -->

		<section id="fifth-screen" class="fifth-screen">
			<div class="container">
				<h3>КОРОТКО О ПРОГРАММАХ</h3>
				<div class="box">
					<div class="box-top">
						<h4><?php echo esc_html( carbon_get_the_post_meta( 'crb_title_junior' ) ); ?></h4>
					</div>
					<div class="box-bottom">
						<div class="left">
							<?php echo wp_get_attachment_image( carbon_get_the_post_meta( 'photo_junior' ), 'full' ); ?>

						</div>
						<div class="right">
							<div class="info">
								<?php echo wp_kses_post( wpautop( carbon_get_the_post_meta( 'crb_info_1_junior' ) ) ); ?>
							</div>
							<div class="info">
								<?php echo wp_kses_post( wpautop( carbon_get_the_post_meta( 'crb_info_2_junior' ) ) ); ?>
							</div>
						</div>
					</div>
					<div class="btns-row">
						<div></div>
						<div>
							<ul class="buttons">
								<li>
									<a href="#course-modal" rel="modal:open" class="btn">
										<?php esc_html_e( 'Записаться на курс', 'a-level-kids' ); ?>
									</a>
								</li>
								<li>
									<a href="#course-modal-free" rel="modal:open" class="btn">
										<?php esc_html_e( 'Попробовать бесплатно', 'a-level-kids' ); ?>
									</a>
								</li>
							</ul>
						</div>

						<div class="show-all">
							<a
									href="<?php echo esc_html( carbon_get_the_post_meta( 'link_junior' ) ); ?>"
									class="btn btn-trn">
								<?php esc_html_e( 'УЗНАТЬ ПОДРОБНЕЕ', 'a-level-kids' ); ?>
							</a>
						</div>
					</div>
				</div>

				<div class="box">
					<div class="box-top">
						<h4><?php echo esc_html( carbon_get_the_post_meta( 'crb_title_senior' ) ); ?></h4>
					</div>
					<div class="box-bottom">
						<div class="left">
							<?php echo wp_get_attachment_image( carbon_get_the_post_meta( 'photo_senior' ), 'full' ); ?>

						</div>
						<div class="right">
							<div class="info">
								<?php echo wp_kses_post( wpautop( carbon_get_the_post_meta( 'crb_info_1_senior' ) ) ); ?>
							</div>
							<div class="info">
								<?php echo wp_kses_post( wpautop( carbon_get_the_post_meta( 'crb_info_2_senior' ) ) ); ?>
							</div>
						</div>
					</div>
					<div class="btns-row">
						<div></div>
						<div>
							<ul class="buttons">
								<li>
									<a href="#course-modal" rel="modal:open" class="btn">
										<?php esc_html_e( 'Записаться на курс', 'a-level-kids' ); ?>
									</a>
								</li>
								<li>
									<a href="#course-modal-free" rel="modal:open" class="btn">
										<?php esc_html_e( 'Попробовать бесплатно', 'a-level-kids' ); ?>
									</a>
								</li>
							</ul>
						</div>
						<div class="show-all">
							<a
									href="<?php echo esc_url( carbon_get_the_post_meta( 'link_senior' ) ); ?>"
									class="btn btn-trn">
								<?php esc_html_e( 'УЗНАТЬ ПОДРОБНЕЕ', 'a-level-kids' ); ?>
							</a>
						</div>
					</div>
				</div>
			</div>

			<div id="junior-modal" class="modal">
				<div class="form-box">
					<h4>Отправьте заявку</h4>
					<p>наш менеджер свяжется с Вами в ближайшее время</p>
					<?php echo do_shortcode( '[contact-form-7 id="29" title="From home junior"]' ); ?>
				</div>
			</div>

			<div id="senior-modal" class="modal">
				<div class="form-box">
					<h4>Отправьте заявку</h4>
					<p>наш менеджер свяжется с Вами в ближайшее время</p>
					<?php echo do_shortcode( '[contact-form-7 id="30" title="From home senior"]' ); ?>
				</div>
			</div>
		</section><!-- #fifth-screen -->

		<section id="six-screen" class="sixth-screen">
			<div class="container">
				<h3>ЗАЧЕМ НУЖНЫ КУРСЫ ДЛЯ ШКОЛЬНИКОВ В A-LEVEL?</h3>
				<div class="row">
					<div class="col">
						<div class="top">
							<h5><?php echo esc_html( carbon_get_the_post_meta( 'crb_title_six_1' ) ); ?></h5>
						</div>
						<div class="bottom">
							<p><?php echo esc_html( carbon_get_the_post_meta( 'crb_text_six_1' ) ); ?></p>
						</div>
					</div>
					<div class="col">
						<div class="top">
							<h5><?php echo esc_html( carbon_get_the_post_meta( 'crb_title_six_2' ) ); ?></h5>
						</div>
						<div class="bottom">
							<p><?php echo esc_html( carbon_get_the_post_meta( 'crb_text_six_2' ) ); ?></p>
						</div>
					</div>
					<div class="col">
						<div class="top">
							<h5><?php echo esc_html( carbon_get_the_post_meta( 'crb_title_six_3' ) ); ?></h5>
						</div>
						<div class="bottom">
							<p><?php echo esc_html( carbon_get_the_post_meta( 'crb_text_six_3' ) ); ?></p>
						</div>
					</div>
				</div>
			</div>
		</section><!-- #sixth-screen  -->

		<?php get_template_part( 'template-parts/content', 'form' ); ?>

	</main><!-- .main -->

<?php
get_footer();
