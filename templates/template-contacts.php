<?php
/* Template Name: Contacts page
*/

get_header();
?>

    <main class="main contacts-page-main">
        <section class="first-screen">
            <div class="container">
                <div class="left">
                    <h1>НАШИ КОНТАКТЫ</h1>
                    <div class="box-contact">
                        <div class="box-left"></div>
                        <div class="box-right">
                            <?php
                            $phones = carbon_get_the_post_meta('crb_phone');

                            foreach($phones as $item) { ?>
                                <a href="tel: <?php echo $item['text'] ?>"><?php echo $item['text'] ?></a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="box-contact">
                        <div class="box-left">

                        </div>
                        <div class="box-right">
                            <?php echo carbon_get_the_post_meta('crb_address') ?>
                        </div>
                    </div>
                    <div class="box-contact">
                        <div class="box-left">

                        </div>
                        <div class="box-right">
                            <div class="tel">
                                <?php echo carbon_get_the_post_meta('crb_time') ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="right">
                    <img src="<?php echo the_post_thumbnail_url('full'); ?>" alt="robot">
                </div>
            </div>
        </section><!--first-screen -->

        <section id="form-screen" class="second-screen form-screen">
            <div class="container">
                <div class="top">
                    <h3>НУЖЕН ОБРАТНЫЙ ЗВОНОК?</h3>
                    <p>
                        Администратор с удовольствием ответит на все вопросы и проконсультирует по поводу занятий. Все,
                        что нужно - это заполнить форму
                    </p>
                </div>
                <div class="center">
                    <?php echo do_shortcode('[contact-form-7 id="10" title="From ask"]') ?>
                </div>
            </div>
        </section><!-- #form-screen -->


        <section class="third-screen lesson">
            <div class="container">
                <div class="top">
                    <h3>БЛИЖАЙШИЕ ОТКРЫТЫЕ УРОКИ</h3>
                    <p>
                        Цель открытого урока - познакомить юного студента с атмосферой на курсах и вызвать интерес к
                        тому, что его дальше здесь ждет. Открытые уроки бесплатны и их может посетить любой
                        желающий.</p>
                </div>
                <div class="bottom">
                    <div class="row cards-slider">
                        <?php
                        $courses = carbon_get_post_meta(5,'crb_courses');

                        foreach ($courses as $item) {
                            $id = $item['course'][0];

                            $date_now = new DateTime();
                            $date2 = new DateTime($item['date']);

                            if ($date_now < $date2) { ?>
                                <div class="slide-box">
                                    <a href="<?php echo get_permalink( $id['id'] ); ?>" class="card">
                                        <div class="title">
                                            <span><?php echo carbon_get_post_meta($id['id'], 'crb_cat'); ?></span>
                                        </div>
                                        <div class="content">
                                            <div class="card-top">
                                                <div class="left">
                                                    <h6>
                                                        <?php echo get_the_title($id['id']) ?>
                                                    </h6>
                                                </div>
                                                <div class="right">
                                                    <p><?php echo $item['date']; ?></p>
                                                </div>
                                            </div>
                                            <div class="card-bottom">
                                                <?php echo get_the_post_thumbnail($id['id'], 'full'); ?>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php }
                        } ?>
                    </div>
                </div>

            </div>
        </section><!-- .lesson -->
    </main><!-- .main -->

<?php
get_footer();
