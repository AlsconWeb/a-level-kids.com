<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package A-Level_kids
 */

?>

<footer>
    <div class="container">
        <div class="logo">
            <img src="<?php echo get_template_directory_uri() . '/assets/img/logo.png' ?>" alt="A-level logo">
        </div>
        <p><?php echo carbon_get_theme_option('crb_contact') ?></p>
        <div class="social">
            <ul class="list">
                <li class="list-item"><a href="<?php echo carbon_get_theme_option('crb_inst') ?>" class="inst"></a></li>
                <li class="list-item"><a href="<?php echo carbon_get_theme_option('crb_facebook') ?>" class="fb"></a>
                </li>
            </ul>
        </div>

    </div>

    
    <div id="thx-modal" class="modal tnx-modal">
        <div class="form-box">
            <h4>Заявка успешно отправлена</h4>
            <p>Заявка успешно отправлена</p>
            <a href="#" class="btn btn-trn">Назад</a>
        </div>
        <a href="#close-modal" rel="modal:close" class="close-modal ">Close</a>
    </div><!-- onsubmit-modal -->

</footer><!--footer -->
<?php wp_footer(); ?>

</body>
</html>

