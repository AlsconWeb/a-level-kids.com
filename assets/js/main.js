//Menu
function burgerMenu() {
	let menu = jQuery( '.burger-menu' );
	let button = jQuery( '.burger-menu_button' );
	let overlay = jQuery( '.burger-menu_overlay' );

	button.on( 'click', ( e ) => {
		e.preventDefault();
		toggleMenu( menu );
	} );
	overlay.on( 'click', () => toggleMenu( menu ) );
}

function toggleMenu( menu ) {
	menu.toggleClass( 'burger-menu_active' );

	if ( menu.hasClass( 'burger-menu_active' ) ) {
		jQuery( 'body' ).css( 'overlow', 'hidden' );
	} else {
		jQuery( 'body' ).css( 'overlow', 'visible' );
	}
}

jQuery( document ).ready( function( $ ) {
	let title = $( '#lesson h4' );
	let count = $( '.slide-box' );
	if ( count.length ) {
		title.show()
	} else {
		title.hide()
	}
	if ( $( window ).width() < 764 ) {
		$( '.menu-item-has-children' ).each( function() {
			if ( $( this ).find( '.chevron-down' ).length == 0 ) {
				$( this ).children( 'a' ).append( '<i class="chevron-down"></i>' );
			}
		} );
		let flag = false;
		$( '.menu-item-has-children a' ).click( function() {
			if ( ! flag ) {
				$( this ).next( '.sub-menu' ).show();
				flag = ! flag;
			} else {
				$( this ).next( '.sub-menu' ).hide();
				flag = ! flag;
			}
		} );
	}


	burgerMenu();
	$( '.burger-menu_link' ).on( 'click', function( e ) {
		let menu = jQuery( '.burger-menu' );
		let href = jQuery( this ).data( 'link' );


		jQuery( 'html, body' ).animate( {
			scrollTop: jQuery( href ).offset().top - 130
		}, 200 );

		toggleMenu( menu );
	} );
	if ( $( '.cards-slider' ).length ) {
		$( '.cards-slider' ).slick( {
			infinite: true,
			slidesToShow: 3,
			slidesToScroll: 1,
			arrows: true,
			autoplay: true,
			speed: 1000,
			variableWidth: true,
			responsive: [
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 568,
					settings: {
						slidesToShow: 1,
						variableWidth: false,
					}
				}
			]
		} );
	}
	if ( $( '.review-slider' ).length ) {
		$( '.review-slider' ).slick( {
			dots: false,
			infinite: true,
			centerMode: true,
			arrows: true,
			autoplay: true,
			speed: 1000,
			slidesToShow: 3,
			centerPadding: 0,
			responsive: [
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 568,
					settings: {
						slidesToShow: 1,
					}
				}
			]
		} );
	}
	$( '.wpcf7 .btn' ).addClass( 'disabled' );
	let notFilledTel = false,
		notFilledName = false,
		notFilledNameBaby = false,
		notFilledEmail = false,
		notFilledComment = false,
		notFilledCheckbox = false;
	if ( $( '.modal' ).length ) {
		$( '.modal [placeholder="Имя"]' ).keyup( function() {
			if ( $( this ).val().length >= 3 ) {
				notFilledName = true;
				if ( notFilledTel & notFilledName & notFilledNameBaby === true ) {
					$( '.modal .wpcf7 .btn' ).removeClass( 'disabled' );
				}
			} else {
				notFilledName = false;
				$( '.modal .wpcf7 .btn' ).addClass( 'disabled' );
			}
		} );
		$( '.modal [placeholder="Имя ребенка"]' ).keyup( function() {
			if ( $( this ).val().length >= 3 ) {
				notFilledNameBaby = true;
				if ( notFilledTel & notFilledName & notFilledNameBaby === true ) {
					$( '.modal .wpcf7 .btn' ).removeClass( 'disabled' );
				}
			} else {
				notFilledNameBaby = false;
				$( '.modal .wpcf7 .btn' ).addClass( 'disabled' );
			}
		} );
		$( '.modal input[type=tel]' ).inputmask( {
			mask: '+38 (999) 999-99-99',
			oncomplete: function() {
				notFilledTel = true;
				if ( notFilledTel & notFilledName & notFilledNameBaby === true ) {
					$( '.wpcf7 .btn' ).removeClass( 'disabled' );
				}
			},
			onincomplete: function() {
				notFilledTel = false;
				$( '.modal .wpcf7 .btn' ).addClass( 'disabled' );
			},
			oncleared: function() {
				$( '.modal .wpcf7 .btn' ).addClass( 'disabled' );
			},
		} );
	}
	if ( $( '.box-form' ).length ) {
		$( '.box-form [placeholder="Имя"]' ).keyup( function() {
			if ( $( this ).val().length >= 3 ) {
				notFilledName = true;
				if ( notFilledEmail & notFilledName & notFilledComment & notFilledCheckbox === true ) {
					$( '.wpcf7 .btn' ).removeClass( 'disabled' );
				}
			} else {
				notFilledName = false;
				$( '.wpcf7 .btn' ).addClass( 'disabled' );
			}
		} );
		$( '.box-form .wpcf7-email' ).keyup( function() {
			var input = this.value;
			if ( input && /(^\w.*@\w+\.\w)/.test( input ) ) {
				notFilledEmail = true;
				if ( notFilledEmail & notFilledName & notFilledComment & notFilledCheckbox === true ) {
					$( '.wpcf7 .btn' ).removeClass( 'disabled' );
				}
			} else {
				notFilledEmail = false;
				$( '.wpcf7 .btn' ).addClass( 'disabled' );
			}
		} );
		$( '.box-form textarea' ).keyup( function() {
			if ( $( this ).val().length >= 5 ) {
				notFilledComment = true;
				if ( notFilledEmail & notFilledName & notFilledComment & notFilledCheckbox === true ) {
					$( '.wpcf7 .btn' ).removeClass( 'disabled' );
				}
			} else {
				notFilledComment = false;
				$( '.wpcf7 .btn' ).addClass( 'disabled' );
			}
		} );
		$( '.box-form input[type="checkbox"]' ).click( function() {
			if ( $( this ).prop( 'checked' ) === true ) {
				notFilledCheckbox = true;
				if ( notFilledEmail & notFilledName & notFilledComment & notFilledCheckbox === true ) {
					$( '.wpcf7 .btn' ).removeClass( 'disabled' );
				}
			} else {
				notFilledCheckbox = false;
				$( '.wpcf7 .btn' ).addClass( 'disabled' );
			}
		} );
	}
	if ( $( 'input[type=tel]' ).length ) {
		$( '.form-screen [placeholder="Имя"]' ).keyup( function() {
			if ( $( this ).val().length >= 3 ) {
				notFilledName = true;
				if ( notFilledTel & notFilledName === true ) {
					$( '.form-screen .wpcf7 .btn' ).removeClass( 'disabled' );
				}
			} else {
				notFilledName = false;
				$( '.form-screen .wpcf7 .btn' ).addClass( 'disabled' );
			}
		} );
		$( '.form-screen input[type=tel]' ).inputmask( {
			mask: '+38 (999) 999-99-99',
			oncomplete: function() {
				notFilledTel = true;
				if ( notFilledTel & notFilledName === true ) {
					$( '.form-screen .wpcf7 .btn' ).removeClass( 'disabled' );
				}
			},
			onincomplete: function() {
				notFilledTel = false;
				$( '.form-screen .wpcf7 .btn' ).addClass( 'disabled' );
			},
			oncleared: function() {
				$( '.form-screen .wpcf7 .btn' ).addClass( 'disabled' );
			},
		} );
	}

	$( window ).on( 'resize', function() {
		if ( $( window ).width() < 768 ) {
			if ( $( '.text-block' ).length === 0 ) {
				$( '.about-screen .right > p' ).wrapAll( '<div class="text-block"></div>' );
			}
			$( '.box-bottom .info h3' ).each( function() {
				if ( $( this ).parent( '.accordion' ).length === 0 ) {
					let textEl = $( this ).next();
					$( this ).wrap( '<div class="accordion"></div>' ).after( textEl );
					$( '.box-bottom .accordion h3' ).click( function() {
						$( this ).parent().toggleClass( 'open' );
					} );
				}
			} );
		}
	} );
	if ( $( window ).width() < 768 ) {
		$( '.about-screen .right > p' ).wrapAll( '<div class="text-block"></div>' );
		$( '.box-bottom .info h3' ).each( function() {
			let textEl = $( this ).next();
			$( this ).wrap( '<div class="accordion"></div>' ).after( textEl );
		} );
		$( '.box-bottom .accordion h3' ).click( function() {
			$( this ).parent().toggleClass( 'open' );
		} );
	}
	$( '.btn-sound' ).click( function( e ) {
		e.preventDefault();
		mute();
	} );

	function mute() {
		let video = document.getElementById( 'main-video' );
		if ( ! video.muted ) {
			video.muted = true;
			$( '.btn-sound i' ).removeClass();
			$( '.btn-sound i' ).addClass( 'fas fa-volume-mute' );
		} else {
			video.muted = false;
			$( '.btn-sound i' ).removeClass();
			$( '.btn-sound i' ).addClass( 'fas fa-volume-up' );
		}
	}

	const fromFree = document.querySelector( '#wpcf7-f265-o2' );
	const fromCourses = document.querySelector( '#wpcf7-f47-o1' );
	const remainForm = document.querySelector( '#wpcf7-f10-o5' );

	fromFree.addEventListener( 'wpcf7mailsent', function( event ) {
		$.modal.close();
		$( '#course-modal-free-tnx' ).modal();
	}, false );

	fromCourses.addEventListener( 'wpcf7mailsent', function( event ) {
		$.modal.close();
		$( '#course-modal-tnx' ).modal();
	}, false );

	remainForm.addEventListener( 'wpcf7mailsent', function( event ) {
		$.modal.close();
		$( '#remain-modal-tnx' ).modal();
	}, false );
} );