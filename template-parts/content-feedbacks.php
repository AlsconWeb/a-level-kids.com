<?php
/**
 * Template part for displaying section feedbacks
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package A-Level_kids
 */

?>

<section id="review-screen"  class="review-screen">
    <div class="container">
        <h3>ОТЗЫВ О НАС</h3>
        <div class="content review-slider">
            <?php
            $feedbacks = carbon_get_theme_option('crb_feedbacks');
            $output = array_slice($feedbacks, 0, 3);   // возвращает "a", "b" и "c"

            foreach ($output as $item) { ?>
                <div class="col">
                    <div class="top">
                        <div class="left">
                            <?php echo wp_get_attachment_image($item['photo'], 'full'); ?>

                            <div class="box">
                                <p><?php echo $item['name'] ?></p>
                            </div>

                        </div>
                        <div class="right">
                            <p><?php echo $item['date'] ?></p>
                        </div>
                    </div>
                    <div class="bottom">
                        <?php echo wpautop($item['text']) ?>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="btns-row">
            <div></div>
            <div>
                <a href="#feedback-modal" rel="modal:open" class="btn">ОСТАВИТЬ ОТЗЫВ</a>
            </div>
            <div class="show-all">
                <a href="/feedbacks" class="btn btn-trn">ЧИТАТЬ ВСЕ ОТЗЫВЫ</a>
            </div>
        </div>
    </div>

    <div id="feedback-modal" class="modal">
        <div class="form-box">
            <?php echo do_shortcode('[contact-form-7 id="31" title="From feedback"]') ?>
        </div>
    </div>
</section>

