<?php
/**
 * Template part for displaying section feedbacks
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package A-Level_kids
 */

?>

<section id="form-screen" class="form-screen">
    <div class="container">
        <div class="top">
            <h3><?php echo carbon_get_theme_option('form_title') ?></h3>
            <p><?php echo carbon_get_theme_option('form_text') ?></p>
        </div>
        <div class="center">
            <?php echo do_shortcode('[contact-form-7 id="10" title="From ask"]') ?>
        </div>
        <div class="bottom">
            <?php echo wpautop(carbon_get_theme_option('crb_contact_form')) ?>
        </div>
    </div>
</section><!-- #form-screen -->

